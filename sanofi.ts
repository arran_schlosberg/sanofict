module Sanofi.CT {

   export interface IScope extends ng.IScope, IPaginated {
      search: (qry?:IQuery)=>ng.IHttpPromise<IResults>;
      searching: boolean; //disable concurrent searching
      searched: boolean; //only display previous / next buttons if appropriate
      lastQry: IQuery; //allow for pagination without rebuilding the query
      error: boolean;
      errorData: IResults;
      results: IResults;
      aussieLocation: ILocation; //for AngularJS filtering before location searching by post code is implemented
      next: ()=>void; //update the skip & limit values of IPaginated
      prev: ()=>void;
      eligibilityFiltering: ()=>void;
      filtering: boolean;
      filtered: boolean;
      nextCriterion: (applies?:boolean)=>void;
      currentCriterion: ()=>string;
      Math: Math; //expose the window object to Angular
   }
   
   export interface IResults { //return count + trials if successful; msg otherwise
      [index: string]: any;
      count?: number;
      trials?: ITrials;
      exclusionCriteria:number;
      msg?: string;
   }
   
   interface ITrials {
      [index: number]: ITrial;
   }
   
   export interface ITrial {
      exclusion: Array<IExclusionCriterion>
   }
   
   export interface IExclusionCriterion {
      criterion: string;
      applies?:boolean;  
   }
   
   export interface IQuery extends IPaginated {
      [index: string]: any;
      q: string;
      dob: string;
      gender: string;
   }
   
   export interface ILocation {
      facility: {address: {country: string;};};
   }
   
   interface IPaginated {
      skip: number;
      limit: number;
   }
}

class SanofiException implements Error {
   public name:string = 'SanofiException';
   constructor(public message?:string, public meta?:any){}
   public toString():string {
      return this.message;
   }
}

function ThrowSanofi(e:Error, is:boolean = true):void {
   if((e instanceof SanofiException)==is){
      throw e;
   }
}

function ThrowNonSanofi(e:Error):void {
   ThrowSanofi(e, false);
}


(function(){
   "use strict";
   
   var sanofiCTApp = angular.module('sanofiCTApp', []);
   
   /*
    * ###########################################
    *
    * IMPORTANT: use this with caution as it exposes to XSS. 
    * It is currently only used with elements that are escaped server-side with htmlentities() in search.php.
    * 
    * ###########################################
    */
   sanofiCTApp.filter('rawHTML', function($sce:ng.ISCEService){
   	return $sce.trustAsHtml;
   });
   
   sanofiCTApp.filter('joinIfArray', function(){
   	return function(val:any, separator:string){
   		if((typeof val)=='undefined'){
   			return null;
   		}
   		return (typeof val.join)=='function' ? val.join(separator || '; ') : val;
   	}
   });
   
   sanofiCTApp.controller('trialCtrl', ['$scope', '$http', function($scope:Sanofi.CT.IScope, $http:ng.IHttpService){
   	
      if(false){ //useful for testing
         $scope['q'] = 'prostate';
         $scope['comorbid'] = 'cancer';
      }
      
   	$scope.searching = false;
   	$scope.searched = false;
   	$scope.lastQry = <Sanofi.CT.IQuery> {};
   	
   	$scope.results = <Sanofi.CT.IResults> {};
   	$scope.error = false;
   	$scope.errorData = null;
      
      $scope.filtering = false;
      $scope.filtered = false;
   	
   	$scope.skip = 0;
   	$scope.limit = 10;
   	
   	$scope.aussieLocation = {
   		facility : {
   			address : {
   				country : 'Australia'
   			}
   		}
   	};
   	
   	$scope.next = function(){
   		$scope.skip += $scope.limit;
   		$scope.skip = Math.min($scope.skip, Math.floor($scope.results.count % $scope.limit) * $scope.limit);
   		$scope.search();
   	}
   	
   	$scope.prev = function(){
   		$scope.skip -= $scope.limit;
   		$scope.skip = Math.max($scope.skip, 0);
   		$scope.search();
   	}
   	
      var criterion:{trialIdx:number; idx: number};
   	$scope.search = function(newSearch:Sanofi.CT.IQuery = null):ng.IHttpPromise<Sanofi.CT.IResults> {
   		$scope.searching = true;
   
   		if(newSearch){ //generally when explicitly clicking the search button
   			$scope.skip = 0;
            criterion = {
               trialIdx: 0,
               idx: 0
            }
            $scope.filtered = $scope.filtering = false;
   		
   			var qry = <Sanofi.CT.IQuery> {};
   			var fields = ['q', 'dob', 'gender', 'comorbid'];
   			for(var f in fields){
   				var fld = fields[f];
   				if(fld in $scope){
   					qry[fld] = $scope[fld];
   				}
   			}
   			$scope.lastQry = qry;
   		}
   		else {
   			var qry = $scope.lastQry;
   		}
   		
   		qry.skip = $scope.skip;
   		qry.limit = $scope.limit;
   		
         var promise = $http({
   			url: './search.php',
   			method: 'GET',
   			params: qry
   		});
         
   		promise.success(function(data:Sanofi.CT.IResults){
   			$scope.searching = $scope.error = false;
   			$scope.searched = true;
   			$scope.results = data;
            if(data.exclusionCriteria>0){
               $scope.eligibilityFiltering();
            }
   		})
   		.error(function(data){
   			$scope.searching = false;
   			$scope.error = true;
   			$scope.errorData = data;
   		});
         
         return promise;
   	}
      
      $scope.eligibilityFiltering = function():void {
         (<any>$('#exclusion')).modal();
      }
      
      $scope.nextCriterion = function(applies:boolean = null):void {
         $scope.results.trials[criterion.trialIdx].exclusion[criterion.idx].applies = applies;
         if(applies){
            criterion.trialIdx++;
            criterion.idx = -1;
         }
         criterion.idx++;
      }
      
      $scope.currentCriterion = function():string {
         var found = false;
         try {
            while($scope.results.trials[criterion.trialIdx]){
               if($scope.results.trials[criterion.trialIdx].exclusion[criterion.idx]){
                  throw new SanofiException();
               }
               criterion.trialIdx++;
               criterion.idx = 0;
            }
            $scope.filtered = true;
         }
         catch(e){
            ThrowNonSanofi(e); //i.e. a real error, not a means of escaping the try block
            found = true;
         }
         
         if(!found){
            return null;
         }
         
         return $scope.results.trials[criterion.trialIdx].exclusion[criterion.idx].criterion;
      }
   	
   	$scope.Math = (<any>window).Math; //<any> makes TypeScript play nicely
   }]);
})();