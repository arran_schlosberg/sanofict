<?php
namespace Oonix\Sanofi\ClinicalTrials;

try {

	require "./vendor/autoload.php";
	define('LOCAL', $_SERVER['HTTP_HOST']=='sanofi-trials.localhost');
	$m = (new \MongoClient(LOCAL ? null : "mongodb://localhost:27018"))->sanofi->clinical_trials;
	TrialFactory::init($m);
	
	$qry = []; //for use in MongoCollection->find
	
	foreach($_GET as $k => $v){
		$v = trim($v);
		if(empty($v)){
			unset($_GET[$k]);
		}
		else {
			$_GET[$k] = $v;
		}
	}
	
	if(isset($_GET['q'])){
		$q = trim($_GET['q']);
		
		// https://php.net/preg-split#92632 (modified to exclude commas as we handle them later) - split by whitespace, but respect quotes
		$raw = preg_split("/[\s]*\\\"([^\\\"]+)\\\"[\s]*|" . "[\s]*'([^']+)'[\s]*|" . "[\s]+/", $q, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
		
		// group the adjacent raw parts that are "standalone" (i.e. no spaces) into strings, but separate those that were originally quoted
		$parts = [];
		$newPart = true; //whether or not the next raw part should be forced to form a new part
		foreach($raw as $part){
		   $part = trim($part); //I sourced the split RegExp from PHP documentation, and although I think it will implicitly trim the parts, just do it to be sure, to be sure
		   if(preg_match("/\s/", $part)){ //this must have been a quoted string so handle it by itself, and force the next part to be grouped separately
		      $parts[] = $part;
		      $newPart = true;
		   }
		   else { //append it to the previous one if it exists
		      $newPartNextTime = false;
		      if(substr($part, -1)==","){
		         $newPartNextTime = true;
		         $part = substr($part, 0, -1);
		      }
		      
		      if($newPart){
		         $parts[] = $part;
		      }
		      else {
		         $last = array_pop($parts);
		         $parts[] = "{$last} {$part}";
		      }
		      
		      $newPart = $newPartNextTime;
		   }
		}
		
		// MongoDB only respects double quotes, so apply them to our groups defined above
		foreach($parts as $k => $v){
		   $parts[$k] = '"'.$v.'"';
		}
		// https://stackoverflow.com/questions/23084243/php-mongodb-full-text-search-and-sort
		$qry['$text'] = array('$search' => implode(",", $parts));
	}

	if(isset($_GET['dob'])){
		$parts = explode("/", $_GET['dob']);
		foreach($parts as $idx => $p){
			if(!ctype_digit($p)){
				throw new CTException("Invalid date of birth. Format must be: dd/mm/yyyy");
			}
			$parts[$idx] = (int) $p;
		}
		if($parts[1]<1 || $parts[1]>12){
			throw new CTException("Invalid month in the date of birth.");
		}
		$date = implode("-", array_reverse($parts));
		$dob = new \MongoDate(strtotime($date));
		
		$qry['parsed.born_before'] = ['$gte' => $dob];
		$qry['parsed.born_after'] = ['$lte' => $dob];
	}
	
	if(isset($_GET['gender']) && in_array($_GET['gender'], ['Female', 'Male'])){
		$qry['raw.eligibility.gender'] = ['$in' => ['Both', $_GET['gender']]];
	}
	
	if(isset($_GET['healthy']) && $_GET['healthy']){
		$qry['raw.eligibility.healthy_volunteers'] = ['$eq' => 'Accepts Healthy Volunteers'];
	}
	
	function intOrZero($val){
		return ctype_digit($val) ? (int) $val : 0;
	}
	
	$results = $m->find($qry,
		[
			'_id',
			'raw.brief_title',
			'raw.brief_summary.textblock',
			'raw.eligibility',
			'raw.condition_browse.mesh_term',
			'raw.location',
			'score' => array('$meta' => 'textScore')
		]
	)->sort(
		['score' => array('$meta' => 'textScore')]
	)->skip(intOrZero(@$_GET['skip']))->limit(intOrZero($_GET['limit']));
	
	$arr = [];
	foreach($results as $r){
		$new = $r['raw'];
		$new['id'] = is_array($r['_id']) ? $r['_id'][0] : $r['_id'];
		$new['score'] = $r['score'];
		$new['exclusion'] = [];
		$arr[$new['id']] = $new;
	}
	
	array_walk_recursive($arr, function (&$v, $k){
		if($k=='textblock'){
			$v = nl2br(htmlentities(trim($v)));
		}
	});
	
	$debug = [];
	$totalCriteria = 0;
	if(isset($_GET['comorbid']) && !empty($_GET['comorbid'])){
      $criteria = TrialFactory::criteriaCollection()->find([
            '$and' => [
               ['trial_id' => ['$in' => array_keys($arr)]],
               ['inclusion' => ['$eq' => 0]],
               ['$text' => ['$search' => $_GET['comorbid']]]
            ]
         ], [
            'trial_id',
            'criterion',
            'score' => array('$meta' => 'textScore')
         ])->sort(['score' => array('$meta' => 'textScore')]);
      
      $totalCriteria = $criteria->count();
      
      $debug[] = $criteria->count();
      
      foreach($criteria as $c){
         $id = $c['trial_id'];
         unset($c['trial_id']);
         $arr[$id]['exclusion'][] = $c;
      }
	}
	
	echo json_encode(['count' => $results->count(), 'trials' => array_values($arr), 'exclusionCriteria' => $totalCriteria, 'debug' => array_unique($debug)]);
}
catch(\Exception $e){
	$internal = is_a($e, "Oonix\Sanofi\ClinicalTrials\CTException");
	http_response_code($internal ? 400 : 418);
	echo json_encode(["msg" => $internal ? $e->getMessage() : null]);
}

?>
