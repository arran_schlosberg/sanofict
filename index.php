<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Sanofi Clinical Trials Search</title>
		<link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="./bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
		<link href="./bower_components/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet">
		<script src="./bower_components/angular/angular.min.js"></script>
		<style>
			a { cursor: pointer; }
			a.btn { display: inline-block!important; }
		</style>
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body ng-app="sanofiCTApp">
		<a name="results"></a>
		<nav class="navbar navbar-default" style="background-color: #fff;">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="/" style="padding: 10px 15px;">
						<img alt="Sanofi" src="/images/logo-30h.png">
					</a>
				</div>
			</div>
		</nav>
		<div ng-controller="trialCtrl">
		
		<div id="exclusion" class="modal fade">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="filtering=false"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">
                     <span ng-show="!filtering"><span class="glyphicon glyphicon-filter"></span> Eligibility Filtering</span>
                     <span ng-show="filtering && !filtered">Does this apply to you?</span>
                     <span ng-show="filtered">Filtering Complete</span>
                  </h4>
               </div>
               <div ng-show="!filtered" class="modal-body">
                  <div ng-hide="filtering">
                     Using your comorbidities we will attempt to filter results based on your eligibility. You will be presented with individual criteria&mdash;please indicate whether or not each one applies to you.
                  </div>
                  <div ng-if="filtering">
                     {{currentCriterion()}}
                  </div>
               </div>
               <div class="modal-footer">
                  <div ng-show="!filtering">
                     <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="filtering=false">Close</button>
                     <button type="button" class="btn btn-primary" ng-click="filtering=true">Start</button>
                  </div>
                  <div ng-show="filtering && !filtered">
                     <button type="button" class="btn btn-default" ng-click="nextCriterion(true)">Yes</button>
                     <button type="button" class="btn btn-default" ng-click="nextCriterion(false)">No</button>
                     <button type="button" class="btn btn-default" ng-click="nextCriterion(null)">I don't know</button>
                  </div>
                  <button ng-show="filtered" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
		
			<div class="container">
				<form class="form-horizontal row">
					<fieldset>
						<div class="row">
							<div class="form-group col-md-6">
								<label class="col-md-4 control-label" for="q">Search</label>  
								<div class="col-md-8">
									<input ng-model="q" id="q" name="q" type="text" placeholder="Condition for treatment" class="form-control input-md">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="col-md-4 control-label" for="q">Comorbities</label>  
								<div class="col-md-8">
									<input ng-model="comorbid" id="comorbid" name="comorbid" type="text" placeholder="Other diseases, separated by commas" class="form-control input-md">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6 disabled">
								<label class="col-md-4 control-label" for="dob">Date of Birth</label>  
								<div class="col-md-8">
									<input ng-model="dob" id="dob" name="dob" type="text" placeholder="dd/mm/yyyy" class="form-control input-md">
								</div>
							</div>
							<div class="form-group col-md-6">
							   <!--
							   The clinical trials database uses 'gender' so it is easier to stick with this for variable naming.
							   However the correct terminology is 'sex' so use it for interface elements.
							   See the WHO explanation at http://www.who.int/gender/whatisgender/en/
							   -->
								<label class="col-md-4 control-label" for="gender">Sex</label>
								<div class="col-md-8"> 
									<label class="radio-inline col-md-3" for="gender-0">
										<input ng-model="gender" type="radio" name="gender" id="gender-0" value="Female">
										Female
									</label> 
									<label class="radio-inline col-md-3" for="gender-1">
										<input ng-model="gender" type="radio" name="gender" id="gender-1" value="Male">
										Male
									</label>
								</div>
							</div>
							<!--<div class="form-group col-md-6">
								<label class="col-md-4 control-label" for="role">I am a </label>
								<div class="col-md-8"> 
									<label class="radio-inline col-md-3" for="role-0">
										<input ng-model="role" type="radio" name="role" id="role-0" value="0">
										Patient
									</label> 
									<label class="radio-inline col-md-3" for="role-1">
										<input ng-model="role" type="radio" name="role" id="role-1" value="1">
										Doctor
									</label>
								</div>
							</div>-->
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<label class="col-md-4 control-label" for="state">Location</label>  
								<div class="col-md-8">
									<select ng-model="state" id="state" name="state" type="text" class="form-control input-md">
										<option value="">All Locations</option>
										<option>Demo only - not yet implemented</option>
									</select>
								</div>
							</div>
							<!--<div class="form-group col-md-6">
								<label class="col-md-4 control-label" for="healthy">Healthy Individual</label>
								<div class="col-md-8"> 
									<label class="radio-inline col-md-3" for="healthy-0">
										<input ng-model="healthy" type="radio" name="healthy" id="healthy-0" value="1">
										Yes
									</label> 
									<label class="radio-inline col-md-3" for="healthy-1">
										<input ng-model="healthy" type="radio" name="healthy" id="healthy-1" value="0">
										No
									</label>
								</div>
							</div>-->
						</div>
						<button ng-disabled="searching" id="search" name="search" ng-cloak class="ng-cloak btn btn-primary pull-right" style="background-color: #515ba2;" ng-click="search(true)">{{(searching && 'Searching...') || 'Search'}}<span class="glyphicon glyphicon-search" style="padding-left: 0.7em;"></span></button>
					</fieldset>
					<hr />
				</form>
				
				<div class="clearfix"></div>
				
				<div id="error" ng-cloak class="ng-cloak" ng-show="error">
					<div class="alert alert-warning" role="alert">{{errorData.msg || 'An error occurred while processing your search. Our developers have been notified and will implement a fix as soon as possible.'}}</div>
				</div>
				
				<?php
				function resultNavigation($toTop=false){
					$href = $toTop ? 'href="#top"' : '';
				?>
				<div ng-cloak class="ng-cloak row" ng-show="searched" style="color: #666;">
					<a <?=$href?> class="btn btn-sm btn-default col-md-1" ng-click="prev()" ng-disabled="skip<=0"</a>Previous</a>
					<div class="col-md-2 text-center" style="height: 30px; line-height: 30px;">Result{{results.count==1 ? '' : 's'}} {{Math.min(skip+1, results.count)}}&#8239;&ndash;&#8239;{{Math.min(skip+limit, results.count)}} of {{results.count}}</div>
					<a <?=$href?> class="btn btn-sm btn-default col-md-1" ng-click="next()" ng-disabled="skip+limit>=results.count">Next</a>
					<a ng-show="results.exclusionCriteria>0" class="btn btn-sm btn-primary col-md-2 pull-right" ng-click="eligibilityFiltering()"><span class="glyphicon glyphicon-filter"></span> Eligibility Filtering</a>
					<div class="clearfix"></div>
					<hr />
				</div>
				<?php
				}
				resultNavigation();
				?>
				
				<div ng-cloak class="ng-cloak row" ng-repeat="t in results.trials">
					<div class="row">
						<div class="col-md-7">
							<h4>
								{{t.brief_title}}
								<br /><a ng-href="https://clinicaltrials.gov/show/{{t.id}}" class="small">&raquo; Full view</a>
							</h4>
							<em>{{t.condition_browse.mesh_term | joinIfArray}}</em>
						</div>
						<div class="col-md-1"></div>
						<div class="col-md-2" ng-show="!lastQry.dob"><em><small>
							Minimum Age: {{t.eligibility.minimum_age}}<br />
							Maximum Age: {{t.eligibility.maximum_age}}
						</small></em></div>
						<div class="col-md-2" ng-show="!lastQry.gender"><em><small>
							Sex: {{t.eligibility.gender}}
						</small></em></div>
					</div>
					<div ng-if="(t.exclusion | filter:{applies: true}).length">
					   <br />
   					<div class="alert alert-warning">
   					   You are not eligible for this study as you answered 'yes' to: <em>{{(t.exclusion | filter:{applies: true})[0].criterion}}</em>
   					</div>
					</div>
					<?php
					$out = [
						['Summary', 'brief_summary.textblock | rawHTML', 'showSummary'],
						['Inclusion / Exclusion Criteria', 'eligibility.criteria.textblock | rawHTML', 'showCriteria']
					];
					foreach($out as $o){
						$flag = "t.angularMeta.{$o[2]}";
					?>
					
					<h5><a ng-click="<?=$flag?> = !<?=$flag?>">&raquo; <?=$o[0]?></a></h5>
					<p class="well" ng-bind-html="t.<?=$o[1]?>" ng-show="<?=$flag?>"></p>
					
					<?php } ?>
					
					<h5><a ng-click="t.angularMeta.showLocations = !t.angularMeta.showLocations">&raquo; Recruitment Locations</a></h5>
					<div ng-show="t.angularMeta.showLocations">
						<address class="well" ng-repeat="l in t.location | filter:aussieLocation | orderBy:'facility.name'">
							<strong>{{l.facility.name || 'Unnamed Facility'}}</strong> <span ng-show="l.status!='Recruiting'"><em>Recruitment status: {{l.status}}</em></span><br />
							{{l.facility.address.city}}<br />
							{{l.facility.address.state}} {{l.facility.address.zip}}
						</address>
					</div>
					
					<hr />
				</div> <!-- ng-repeat results.trials -->
				
				<?php resultNavigation(true); ?>

			</div> <!-- container -->
		</div> <!-- ng-controller -->

		<script src="./bower_components/jquery/dist/jquery.min.js"></script>
		<script src="./bower_components/underscore/underscore-min.js"></script>
		<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="./bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script>
			$(function(){
				$('input[name=dob]').datepicker({
					format: "dd/mm/yyyy",
					startDate: "01/01/1910",
					startView: 2,
					autoclose: true
				});
			});
		</script>
		<script src="./sanofi.js?ver=<?=$_SERVER['HTTP_HOST']=='sanofi-trials.localhost' ? time() : `git log -n 1 --pretty=format:'%h' 'sanofi.js'`;?>"></script>
	</body>
</html>
