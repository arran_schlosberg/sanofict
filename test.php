<?php
namespace Oonix\Sanofi\ClinicalTrials;
header("Content-type: text/plain");

try {
	require "./vendor/autoload.php";
	$m = (new \MongoClient())->sanofi->clinical_trials;

	TrialFactory::init($m, true);
	
	$specific = '';
	$query = empty($specific) ? [] : ['_id.0' => $specific];
	
	$trials = $m->find($query, ['raw.eligibility.criteria.textblock' => true]);
	printf("Total: %u\n", $trials->count());
	
	$count = 0;
	$sectionNames = [1 => 'Inclusion', 2 => 'Exclusion'];
	foreach($trials as $t){
		$id = $t['_id'][0];
		$txt = $t['raw']['eligibility']['criteria']['textblock'];
		if(preg_match("/inclusion criteria(?:[^\n]*):?\n(.*)(?:exclusion criteria:?\n(.*))?/is", $txt, $sections)){
			unset($sections[0]);
			//printf("\n-------\n%s\n-------\n\n%s", $id, $txt);
			//continue;
			foreach($sections as $sNum => $s){
				printf("\n-----\n%s %s\n-----\n", $id, $sectionNames[$sNum]);
				//printf("\n\n%s\n\n", $s);
				$list = preg_split("/(\n|;)\s*[-\d\.]+\s+/is", $s);
				foreach($list as $k => $v){
					$v = trim($v);
					if(empty($v)){
						unset($list[$k]);
					}
					else {
						$list[$k] = preg_replace("/\s{2,}/s", " ", $v);
					}
				}
				var_dump($list);
			}
			$count++;
			//break;
		}
	}
	printf("Matched: %u\n", $count);
}
catch(\Exception $e){
	print_r($e);
}

echo "\n";
?>
