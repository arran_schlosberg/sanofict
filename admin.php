<?php
namespace Oonix\Sanofi\ClinicalTrials;
header("Content-type: text/plain");

try {
	require "./vendor/autoload.php";
	//(new \MongoClient())->sanofi->clinical_trials->drop();
	$m = (new \MongoClient())->sanofi->clinical_trials;

	TrialFactory::init($m, true);
	printf("Fetched %u\n", TrialFactory::fetchAll());
	printf("Search index success: %u\n", TrialFactory::createIndex()['ok']);
	printf("Criteria index success: %u\n", TrialFactory::createCriteriaIndex()['ok']);
	printf("Criteria-type index success: %u\n", TrialFactory::createTrialCriteriaIndex()['ok']);
}
catch(\Exception $e){
	print_r($e);
}
?>
