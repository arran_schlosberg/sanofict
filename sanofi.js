var SanofiException = (function () {
    function SanofiException(message, meta) {
        this.message = message;
        this.meta = meta;
        this.name = 'SanofiException';
    }
    SanofiException.prototype.toString = function () {
        return this.message;
    };
    return SanofiException;
})();
function ThrowSanofi(e, is) {
    if (is === void 0) { is = true; }
    if ((e instanceof SanofiException) == is) {
        throw e;
    }
}
function ThrowNonSanofi(e) {
    ThrowSanofi(e, false);
}
(function () {
    "use strict";
    var sanofiCTApp = angular.module('sanofiCTApp', []);
    sanofiCTApp.filter('rawHTML', function ($sce) {
        return $sce.trustAsHtml;
    });
    sanofiCTApp.filter('joinIfArray', function () {
        return function (val, separator) {
            if ((typeof val) == 'undefined') {
                return null;
            }
            return (typeof val.join) == 'function' ? val.join(separator || '; ') : val;
        };
    });
    sanofiCTApp.controller('trialCtrl', ['$scope', '$http', function ($scope, $http) {
        if (false) {
            $scope['q'] = 'prostate';
            $scope['comorbid'] = 'cancer';
        }
        $scope.searching = false;
        $scope.searched = false;
        $scope.lastQry = {};
        $scope.results = {};
        $scope.error = false;
        $scope.errorData = null;
        $scope.filtering = false;
        $scope.filtered = false;
        $scope.skip = 0;
        $scope.limit = 10;
        $scope.aussieLocation = {
            facility: {
                address: {
                    country: 'Australia'
                }
            }
        };
        $scope.next = function () {
            $scope.skip += $scope.limit;
            $scope.skip = Math.min($scope.skip, Math.floor($scope.results.count % $scope.limit) * $scope.limit);
            $scope.search();
        };
        $scope.prev = function () {
            $scope.skip -= $scope.limit;
            $scope.skip = Math.max($scope.skip, 0);
            $scope.search();
        };
        var criterion;
        $scope.search = function (newSearch) {
            if (newSearch === void 0) { newSearch = null; }
            $scope.searching = true;
            if (newSearch) {
                $scope.skip = 0;
                criterion = {
                    trialIdx: 0,
                    idx: 0
                };
                $scope.filtered = $scope.filtering = false;
                var qry = {};
                var fields = ['q', 'dob', 'gender', 'comorbid'];
                for (var f in fields) {
                    var fld = fields[f];
                    if (fld in $scope) {
                        qry[fld] = $scope[fld];
                    }
                }
                $scope.lastQry = qry;
            }
            else {
                var qry = $scope.lastQry;
            }
            qry.skip = $scope.skip;
            qry.limit = $scope.limit;
            var promise = $http({
                url: './search.php',
                method: 'GET',
                params: qry
            });
            promise.success(function (data) {
                $scope.searching = $scope.error = false;
                $scope.searched = true;
                $scope.results = data;
                if (data.exclusionCriteria > 0) {
                    $scope.eligibilityFiltering();
                }
            }).error(function (data) {
                $scope.searching = false;
                $scope.error = true;
                $scope.errorData = data;
            });
            return promise;
        };
        $scope.eligibilityFiltering = function () {
            $('#exclusion').modal();
        };
        $scope.nextCriterion = function (applies) {
            if (applies === void 0) { applies = null; }
            $scope.results.trials[criterion.trialIdx].exclusion[criterion.idx].applies = applies;
            if (applies) {
                criterion.trialIdx++;
                criterion.idx = -1;
            }
            criterion.idx++;
        };
        $scope.currentCriterion = function () {
            var found = false;
            try {
                while ($scope.results.trials[criterion.trialIdx]) {
                    if ($scope.results.trials[criterion.trialIdx].exclusion[criterion.idx]) {
                        throw new SanofiException();
                    }
                    criterion.trialIdx++;
                    criterion.idx = 0;
                }
                $scope.filtered = true;
            }
            catch (e) {
                ThrowNonSanofi(e);
                found = true;
            }
            if (!found) {
                return null;
            }
            return $scope.results.trials[criterion.trialIdx].exclusion[criterion.idx].criterion;
        };
        $scope.Math = window.Math;
    }]);
})();
//# sourceMappingURL=sanofi.js.map