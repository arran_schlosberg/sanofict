<?php

namespace Oonix\Sanofi\ClinicalTrials;

class DataConverter {

	public static function xmlToObj($string){
		return simplexml_load_string($string);
	}

	public static function xmlToJson($string){
		return json_encode(self::xmlToObj($string));
	}
	
	public static function xmlToArray($string){
		return json_decode(self::xmlToJson($string), true);
	}

	public static function xmlUrlToJson($url){
		return self::xmlToJson(file_get_contents($url));
	}
	
	public static function xmlUrlToArray($url){
		return self::xmlToArray(file_get_contents($url));
	}
	
	public static function xmlUrlToObj($url){
		return self::xmlToObj(file_get_contents($url));
	}
	
	public static function arrayToObj($arr){
		return json_decode(json_encode($arr));
	}
	
	public static function objToArray($obj){
		return json_decode(json_encode($obj), true);
	}

}

?>
