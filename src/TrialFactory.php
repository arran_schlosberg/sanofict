<?php

namespace Oonix\Sanofi\ClinicalTrials;

/*
 * Convenience class to inject the MongoCollection dependency
 */
class TrialFactory {

	private static $_init = false;
	private static $_mongo;
	private static $_verbose;
	
	public static function init(\MongoCollection $mongo, $verbose=false){
		self::$_mongo = $mongo;
		self::$_init = true;
		self::$_verbose = $verbose;
	}
	
	public static function checkInit(){
		if(self::$_init!==true){
			throw new CTException("TrialFactory can not create new Trial without first running ::init().");
		}
	}
	
	public static function ensureIndex(){
		self::createIndex();
	}
	
	public static function createIndex(){
		self::checkInit();
		$keys = ['brief_title', 'official_title', 'brief_summary.textblock', 'detailed_description.textblock', 'eligibility.criteria.textblock', 'keyword', 'condition_browse.mesh_term'];
		$idx = ['_id' => 'text'];
		foreach($keys as $k){
			$idx["raw.{$k}"] = "text";
		}
		return self::createOrEnsureIndex($idx, ['name' => 'textSearch']);
	}
	
	public static function createTrialCriteriaIndex(){
	   self::checkInit();
	   return self::createOrEnsureIndex(['trial_id' => true, 'inclusion' => true], ['name' => 'trialCriteriaTypes'], self::criteriaCollection());
	}
	
	public static function createCriteriaIndex(){
	   self::checkInit();
	   return self::createOrEnsureIndex(['criterion' => 'text'], ['name' => 'criteriaSearch'], self::criteriaCollection());
	}
	
	private static function createOrEnsureIndex($idx, $opt, $mongo = null){
	   if(is_null($mongo)){
	      $mongo = self::$_mongo;
	   }
	   if(method_exists($mongo, "createIndex")){ //as of 1.5.0
	      return $mongo->createIndex($idx, $opt);
	   }
	   else { //deprecated as of 1.5.0
	      return $mongo->ensureIndex($idx, $opt);
	   }
	}
	
	public static function criteriaCollection(){
	   $name = self::$_mongo->getName();
	   return self::$_mongo->db->{"{$name}_criteria"};
	}
	
	public static function create($id, $forceFresh=false){
		self::checkInit();
		return new Trial($id, self::$_mongo, $forceFresh);
	}
	
	public static function fetchAll($limit=999999){
		self::checkInit();
		$all = DataConverter::xmlUrlToObj("http://clinicaltrials.gov/ct2/results?flds=k&flds=p&recr=Open&cntry1=PA%3AAU&displayxml=true&count={$limit}");
		$count = 0;
		$total = (int) $all->attributes()['count'];
		foreach($all->clinical_study as $trial){
			if(self::$_verbose){
				printf("[%u of %u] Trial ID: %s\n", $count, $total, $trial->nct_id);
			}
			$t = self::create($trial->nct_id);
			$t->updateIfOld(); //some may just be sourced from the cache
			$count++;
		}
		return $count;
	}

}

?>
