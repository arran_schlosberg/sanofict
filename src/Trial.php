<?php

namespace Oonix\Sanofi\ClinicalTrials;

define('GENDER_MALE', "Male");
define('GENDER_FEMALE', "Female");

class Trial extends DataConverter {

	private $_id;
	private $_cacheTime;
	private $_data;
	private $_parseTime;
	private $_parsed;
	private $_mongo;

	public function __construct($id, \MongoCollection $mongo, $forceFresh=false){
	   $id = trim((string) $id);
		if(!preg_match("/^NCT\d{8}$/", $id)){
			throw new CTException("Invalid NCT ID provided: {$id}");
		}
		$this->_id = $id;
		$this->_mongo = $mongo;
		$this->getData($forceFresh);
		return $this;
	}
	
	public function getData($forceFresh=false){
		try {
			if($forceFresh){
				throw new CTException("fresh");
			}
			$cached = $this->_mongo->findOne(array('_id' => $this->_id));
			if(!$cached){
				throw new CTException("fresh");
			}
			$this->_parsed = $cached['parsed'];
			$obj = self::arrayToObj($cached);
			$this->_data = $obj->raw;
			$this->_cacheTime = $obj->cacheTime;
			$this->_parseTime = $obj->parseTime;
		}
		catch(CTException $e){
			if($e->getMessage()!=="fresh"){
				throw $e;
			}
			$this->_data = self::xmlURLtoObj($this->url());
			if($this->_data===false){
				throw new CTException("Unable to retrieve fresh data for trial: {$this->_id}.");
			}
			$this->mongoUpdate(array('$set' => array('raw' => $this->_data, 'cacheTime' => time())), array('upsert' => true));
			$this->parse();
		}
		return $this;
	}
	
	private function mongoUpdate($data, $options=null){
		return $this->_mongo->update(array('_id' => $this->_id), $data, is_null($options) ? array() : $options);
	}
	
	public function url(){
		return "http://clinicaltrials.gov/show/{$this->_id}?displayxml=true";
	}
	
	public function parse($full=false){
		$el = $this->_data->eligibility;
		$this->_parsed = array( //changes daily
			'born_before'	=> new \MongoDate(self::parseAge($el->minimum_age, time())),
			'born_after'	=> new \MongoDate(self::parseAge($el->maximum_age, -5364698400)) //1st Jan 1800
		);
		
		if($full){ //data in here is less likely to change
   		/*
   		 * Extract text-based inclusion / exclusion criteria.
   		 */
		   $coll = TrialFactory::criteriaCollection();
		   
   		$txt = $el->criteria->textblock;
   		if(preg_match("/inclusion criteria(?:[^\n]*):?\n(?<inclusion>.*?)exclusion criteria:?\n(?<exclusion>.*)/is", $txt, $sections)){ //matches sections with both headings 'inclusion criteria' and 'exclusion criteria'
   		   foreach(['inclusion', 'exclusion'] as $type){
   		      if(!isset($sections[$type])){
   		         continue;
   		      }
   		      $criteria = preg_split("/(\n|;)\s*[-\d\.]+\s+/is", $sections[$type]);
   		      $inclusion = $type=='inclusion' ? 1 : 0;
   		      foreach($criteria as $k => $v){
   		         $v = trim($v);
   		         if(empty($v)){ //blank line
   		            unset($criteria[$k]);
   		         }
   		         else {
   		            $criteria[$k] = [
   		               'trial_id' => $this->_id,
   		               'inclusion' => $inclusion,
   		               'criterion' => preg_replace("/\s{2,}/s", " ", $v) //clean up whitespace (e.g. new lines followed by tabs)
   		            ];
   		         }
   		      }
   		      
   		      $coll->remove(['$and' => [['trial_id' => $this->_id], ['inclusion' => $inclusion]]]);
   		      $coll->batchInsert($criteria);
   		   }
   		}
		}
		
		$this->_parseTime = time();
		$this->mongoUpdate(array('$set' => array(
		      'parseTime' => $this->_parseTime,
		      'parsed' => $this->_parsed
		)));
		
		return $this;
	}

	public function rawData(){
		return $this->_data;
	}
	
	public function parsedData(){
		return $this->_parsed;
	}
	
	public static function parseAge($age, $default){
		return $age=='N/A' ? $default : strtotime("-{$age}");
	}
	
	public function currentVersionTime(){
		return strtotime($this->_data->lastchanged_date);
	}
	
	public function latestVersionTime(){
		$latest = self::xmlUrlToObj("http://clinicaltrials.gov/ct2/results?id={$this->_id}&flds=p&displayxml=true");
		$num = (int) $latest->attributes()->count;
		$obj = $latest->clinical_study;
		if($num!==1){
			foreach($latest->clinical_study as $cs){
				if($cs->nct_id!=$this->_id){
					continue;
				}
				$obj = $cs;
				break;
			}
		}
		if($obj->nct_id->asXML()!=$this->_id->asXML()){
			throw new CTException("Search results for update time of trial '{$this->_id}' did not return any results for this trial.");
		}
		return strtotime($obj->last_changed);
	}
	
	public function update(){
		if($this->latestVersionTime() > $this->currentVersionTime()){
			$this->getData(true);
		}
		else {
			$this->_cacheTime = time();
			$this->mongoUpdate(array('$set' => array('cacheTime' => $this->_cacheTime)));
		}
		return $this;
	}
	
	public function updateIfOld($days=14){
		if($this->_cacheTime && ((time() - $this->_cacheTime) > $days * 86400)){
			$this->update();
		}
		return $this;
	}

	public function meshCondition(){
		return $this->_data->condition_browse->mesh_term;
	}
	
	public function keywords(){
		return $this->_data->keyword;
	}
	
	public function genderEligible($gender){
		if($gender!==GENDER_MALE && $gender!==GENDER_FEMALE){
			throw new CTException("Invalid gender. Must be one of the constants GENDER_MALE or GENDER_FEMALE.");
		}
		$g = $this->_data->eligibility->gender;
		return $g=="Both" || $g==$gender;
	}
	
	public function ageEligible(\DateTime $dob){
		$dob = $dob->getTimestamp();
		return $this->_parsed['born_before']->sec >= $dob && $this->_parsed['born_after']->sec <= $dob;
	}
	
	public function healthyEligible($healthy=true){
		$healthy = $healthy ? "Yes" : "No";
		return $this->_data->eligibility->healthy_volunteers===$healthy;
	}

}

?>
